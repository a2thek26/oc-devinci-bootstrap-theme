/*
 * ========================
 * APP
 * ========================
 */

'use strict';

if( ! window.console) var console = { log: function(){} };

/**
 * Define App Class
 * @constructor
 */
var App = function(){
    this.init();
};

/**
 * Initialize the App
 */
App.prototype.init = function() {

};

/**
 * Set global notify action
 * @param type
 * @param message
 * @param delay
 */
App.prototype.notify = function(type, message, delay) {

};

/**
 * Create an instance
 * @type {App}
 */
var app = new App();


